package ru.kuzin.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kuzin.tm.api.service.dto.ITaskDtoService;
import ru.kuzin.tm.dto.model.TaskDTO;
import ru.kuzin.tm.enumerated.EntitySort;
import ru.kuzin.tm.enumerated.Status;
import ru.kuzin.tm.exception.entity.TaskNotFoundException;
import ru.kuzin.tm.exception.field.*;
import ru.kuzin.tm.repository.dto.ITaskDtoRepository;

import java.util.Collections;
import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class TaskDtoService implements ITaskDtoService {

    @NotNull
    @Autowired
    private ApplicationContext context;

    @NotNull
    @Autowired
    private ITaskDtoRepository repository;

    @Override
    @SneakyThrows
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.deleteAllByUserId(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public List<TaskDTO> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull List<TaskDTO> result;
        result = repository.findAllByUserId(userId);
        return result;
    }

    @Override
    @SneakyThrows
    @Transactional
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        boolean result;
        result = repository.existsByUserIdAndId(userId, id);
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable TaskDTO result;
        result = repository.getOneByUserIdAndId(userId, id);
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 1) throw new IndexIncorrectException();
        @Nullable TaskDTO result;
        @Nullable final List<TaskDTO> taskList;
        final Pageable pageable = PageRequest.of(index - 1, 1);
        taskList = repository.getOneByIndexAndUserId(userId, pageable);
        if (taskList == null || taskList.isEmpty()) return null;
        result = taskList.get(0);
        return result;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        repository.deleteByUserIdAndId(userId, id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 1) throw new IndexIncorrectException();
        @Nullable TaskDTO task;
        @Nullable final List<TaskDTO> taskList;
        final Pageable pageable = PageRequest.of(index - 1, 1);
        taskList = repository.getOneByIndexAndUserId(userId, pageable);
        if (taskList == null || taskList.isEmpty()) throw new TaskNotFoundException();
        task = taskList.get(0);
        repository.deleteByUserIdAndId(userId, task.getId());
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    @SuppressWarnings({"unchecked"})
    public List<TaskDTO> findAll(@Nullable final String userId, @Nullable final EntitySort entitySort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull List<TaskDTO> result;
        final Sort sortable = Sort.by(Sort.Direction.ASC, entitySort == null ? EntitySort.BY_CREATED.getSortField() : entitySort.getSortField());
        result = repository.findAllSortByUserId(userId, sortable);
        return result;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void changeTaskStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final TaskDTO task;
        task = repository.getOneByUserIdAndId(userId, id);
        if (task == null) throw new TaskNotFoundException();
        if (status == null) throw new StatusEmptyException();
        task.setStatus(status);
        repository.save(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void changeTaskStatusByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 1) throw new IndexIncorrectException();
        @Nullable final TaskDTO task;
        @Nullable final List<TaskDTO> taskList;
        final Pageable pageable = PageRequest.of(index - 1, 1);
        taskList = repository.getOneByIndexAndUserId(userId, pageable);
        if (taskList == null || taskList.isEmpty()) throw new TaskNotFoundException();
        task = taskList.get(0);
        if (status == null) throw new StatusEmptyException();
        task.setStatus(status);
        repository.save(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public List<TaskDTO> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        @Nullable final List<TaskDTO> result;
        result = repository.findAllByUserIdAndProjectId(userId, projectId);
        return result;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable TaskDTO task = new TaskDTO();
        task.setName(name);
        task.setUserId(userId);
        if (description != null && !description.isEmpty())
            task.setDescription(description);
        repository.save(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void add(@Nullable final String userId, @Nullable final TaskDTO task) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (task == null) throw new TaskNotFoundException();
        task.setUserId(userId);
        repository.save(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable TaskDTO task;
        task = repository.getOneByUserIdAndId(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        if (description != null && !description.isEmpty())
            task.setDescription(description);
        repository.save(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 1) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable TaskDTO task;
        @Nullable final List<TaskDTO> taskList;
        final Pageable pageable = PageRequest.of(index - 1, 1);
        taskList = repository.getOneByIndexAndUserId(userId, pageable);
        if (taskList == null || taskList.isEmpty()) throw new TaskNotFoundException();
        task = taskList.get(0);
        task.setName(name);
        if (description != null && !description.isEmpty())
            task.setDescription(description);
        repository.save(task);
    }

}