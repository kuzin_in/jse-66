package ru.kuzin.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public class AbstractUserOwnedModel extends AbstractModel {

    @Nullable
    @ManyToOne
    @JoinColumn(nullable = false, name = "user_id")
    private User user;

}