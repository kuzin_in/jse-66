package ru.kuzin.tm.listener;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.kuzin.tm.api.ILoggerService;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

@Component
public final class LoggerListener implements MessageListener {

    @Autowired
    private ILoggerService loggerService;

    @Override
    @SneakyThrows
    public void onMessage(Message message) {
        if (!(message instanceof TextMessage)) return;
        @NotNull final TextMessage textMessage = (TextMessage) message;
        @NotNull final String text = textMessage.getText();
        loggerService.writeLog(text);
    }

}