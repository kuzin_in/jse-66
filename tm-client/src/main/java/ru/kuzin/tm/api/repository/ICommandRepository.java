package ru.kuzin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kuzin.tm.listener.AbstractListener;

import java.util.Collection;

public interface ICommandRepository {

    void add(@Nullable AbstractListener command);

    @Nullable
    AbstractListener getCommandByArgument(String argument);

    @Nullable
    AbstractListener getCommandByName(String name);

    @NotNull
    Iterable<AbstractListener> getCommandsWithArgument();

    @NotNull
    Collection<AbstractListener> getTerminalCommands();

}