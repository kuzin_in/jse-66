package ru.kuzin.tm.exception;

import org.jetbrains.annotations.NotNull;

public class AbstractException extends RuntimeException {

    public AbstractException(@NotNull String message) {
        super(message);
    }

}