package ru.kuzin.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.kuzin.tm.model.ProjectDTO;

@Repository
public interface IProjectRepository extends JpaRepository<ProjectDTO, String> {
}