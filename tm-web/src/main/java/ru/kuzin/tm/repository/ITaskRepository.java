package ru.kuzin.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.kuzin.tm.model.TaskDTO;

@Repository
public interface ITaskRepository extends JpaRepository<TaskDTO, String> {
}